# Ansible Script for Preparing Django Server with postgis(Ubuntu 20.04)

### How To use

1. `files` directory

    ```
    This directory include .pub file.
    To make it easy name your pub file with your name of user that you want to create.
    ```

2. `inventory` directory

    ```
    This directory include hosts file.

    ```

    - Update `ansible_host value` with your server

        ```
        default ansible_host=127.0.0.1
        ```

    - Update `ansible_ssh_pass` with you server password

        ```
        ansible_ssh_pass=your_password
        ```

3. `roles` directory

    ```
    This directory include some of sub directory and file within. No need to change anything on this file,
    but if you need to install specific version of postgresql you need to update packages/tasks/01-postgresql.yml.
    Default is the latest postgresql(currently version 12) + postgis
    ```

    - Setup specific version of postgresql

        ```
            - name: Installing postgis and postgresql
              apt:
                name:
                  - postgis
                  - postgresql-9.6
                  - postgresql-contrib-9.6
                  - postgresql-9.6-postgis-2.5
                state: present
                update_cache: true
              become: true
        ```

4. `vars` directory

    ```
    This directory include sub directory 'all' and some yml file.
    In this case there are 2 files database.yml and users.yml.
    ```

    - Update `database.yml` file with db credential that you want to create

    - Update `users.yml` file with user that you want to create(same name with the .pub file that i mention before.)

5. `server-playbook.yml` file

    ```
    This file is a "root" script, you can edit whatever roles that you want to execute when you run the script.
    ```

6. Run

    ```
    $ ansible-playbook --inventory-file=inventory/hosts server-playbook.yml
    ```

### You want to run this script again

1. Please check your user has been created or not if:

    - Yes, you need to update `hosts` file under `inventory` directory
        - Update `ansible_user` with you user that you have been set on `users.yml`
            ```
            ansible_user=your_user
            ```

        - Update `ansible_ssh_pass`
            ```
            ansible_ssh_private_key_file=~/.ssh/id_rsa
            ```

    - No, Feel free to go


    ```
    $ ansible-playbook --inventory-file=inventory/hosts server-playbook.yml
    ```